plugins {
    kotlin("jvm") version "1.6.10-RC"
    java
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    implementation(project(":test"))
    implementation(project(":test2"))
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}